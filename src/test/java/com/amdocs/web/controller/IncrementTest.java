package com.amdocs;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import com.amdocs.Increment;

public class IncrementTest {
	
    @Test
    public void testGetCounter() {
        final Increment increment = new Increment();
        final int expected = 1; // As per your getCounter method, it should return 1 at first call
		
        assertEquals("testGetCounter, result", expected, increment.getCounter());
    }

    @Test
    public void testDecreaseCounter() {
        final Increment increment = new Increment();
        final int expected = 1; // As per your decreasecounter method, it should return 1 when input is not 0 or 1
		
        assertEquals("testDecreaseCounter, result",expected, increment.decreasecounter(2));
    }

    @Test
    public void testDecreaseCounterZero() {
        final Increment increment = new Increment();
        final int expected = 1; // As per your decreasecounter method, it should return 1 when input is not 0 or 1
		
        assertEquals("testDecreaseCounter, result",expected, increment.decreasecounter(0));
    }

    @Test
    public void testDecreaseCounterOne() {
        final Increment increment = new Increment();
        final int expected = 1; // As per your decreasecounter method, it should return 1 when input is not 0 or 1
		
        assertEquals("testDecreaseCounter, result",expected, increment.decreasecounter(1));
    }
}
