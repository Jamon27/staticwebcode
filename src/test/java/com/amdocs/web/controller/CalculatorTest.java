package com.amdocs;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import com.amdocs.Calculator;

public class CalculatorTest {
	
    @Test
    public void testAdd() {
        final Calculator calculator = new Calculator();
        final int expected = 9; // As per your add method, 3 + 6 = 9
		
        assertEquals("Result", expected, calculator.add());
    }

    @Test
    public void testSub() {
        final Calculator calculator = new Calculator();
        final int expected = 3; // As per your sub method, 6 - 3 = 3
		
        assertEquals("Result", expected, calculator.sub());
    }
}
